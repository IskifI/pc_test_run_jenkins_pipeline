parameters {
    // Объявляем переменные типа boolean. Если записать значения типа boolean в переменную обычным способом (env.var = value.toBoolean()), то переменная всё равно будет типа string
    booleanParam(name: 'booleanHttpsProtocol', defaultValue: '', description: '')
    booleanParam(name: 'booleanStatusBySLA', defaultValue: '', description: '')
    booleanParam(name: 'booleanVudsMode', defaultValue: '', description: '')
}

def getTestParams (testName) {
    def empty = ''

    // В эту переменную будут записываться выводы powershell-скриптов
    def value = empty

    // Переменные, значения которых остаются пустыми при запуске теста. Далее они закомментированы. Если в конфиге их значения изменятся на непустые, переменные нужно раскомментировать.
    env.proxyOutPassword = empty
    env.proxyOutUrl = empty
    env.proxyOutUser = empty
    env.description = empty
    env.addRunToTrendReport = empty
    env.trendReportId = empty
    env.testInstanceId = empty

    // Записываем название теста в глобальную перменную, чтобы его можно было прочитать в powershell-скрипте
    env.var = testName

    // Считываем xml файл в переменную
    //def xml = readFile "${env.WORKSPACE}/MobileShop_Config.xml"

    // Сначала общие параметры
    // Берём сервер и порт Jenkins
    value = sh (script: '''xmllint --xpath '//MobileShop/tests/generalSettings/serverAndPort/text()' MobileShop_Config.xml''',,returnStdout:true).trim()
    env.serverAndPort = value
    echo env.serverAndPort

    // Берём адрес Performance Center
    value = sh (script: '''xmllint --xpath '//MobileShop/tests/generalSettings/pcServerName/text()' MobileShop_Config.xml''',,returnStdout:true).trim()
    env.pcServerName = value
    echo env.pcServerName

    // Подключение к Performance Center использует https?
    value = sh (script: '''xmllint --xpath '//MobileShop/tests/generalSettings/httpsProtocol/text()' MobileShop_Config.xml''',,returnStdout:true).trim()
    booleanHttpsProtocol = value.toBoolean()
    echo env.booleanHttpsProtocol.toString()

    /* Настройк proxyOutPassword
    // Берём адрес proxy
    value = powershell (returnStdout: true, script: '''
        # Открываем конфигурационный файл
        [xml]$configFile = Get-Content $ENV:WORKSPACE/MobileShop_Config.xml
        
        # Возвращаем параметр
        $output = $configFile.MobileShop.tests.generalSettings.proxyOutUrl
        Write-Output "$output)
    ''')
    env.proxyOutUrl = value
    // echo env.proxyOutUrl

    // Берём логин proxy
    value = powershell (returnStdout: true, script: '''
        # Открываем конфигурационный файл
        [xml]$configFile = Get-Content $ENV:WORKSPACE/MobileShop_Config.xml
        
        # Возвращаем параметр
        $output = $configFile.MobileShop.tests.generalSettings.proxyOutUser
        Write-Output "$output)
    ''')
    env.proxyOutUser = value
    // echo env.proxyOutUser

    // Берём пароль proxy
    value = powershell (returnStdout: true, script: '''
        # Открываем конфигурационный файл
        [xml]$configFile = Get-Content $ENV:WORKSPACE/MobileShop_Config.xml
        
        # Возвращаем параметр
        $output = $configFile.MobileShop.tests.generalSettings.proxyOutPassword
        Write-Output "$output)
    ''')
    env.proxyOutPassword = value
    // echo env.proxyOutPassword
    */

    // Берём имя домена для Performance Center
    value = sh (script: '''xmllint --xpath '//MobileShop/tests/generalSettings/almDomain/text()' MobileShop_Config.xml''',,returnStdout:true).trim()
    env.almDomain = value
    echo env.almDomain

    // Берём имя проекта для Performance Center
    value = sh (script: '''xmllint --xpath \'//MobileShop/tests/generalSettings/almProject/text()' MobileShop_Config.xml''',,returnStdout:true).trim()
    env.almProject = value
    echo env.almProject


    // Уникальные параметры теста
    // Берём id теста
    value = sh (script: "xmllint --xpath \'//MobileShop/tests/" + env.var + "/id/text()\' MobileShop_Config.xml",,returnStdout:true).trim()
    env.id = value
    echo env.id

    // Берём количество часов для timeslot
    value = sh (script: "xmllint --xpath \'//MobileShop/tests/" + env.var + "/timeslot/hours/text()\' MobileShop_Config.xml",,returnStdout:true).trim()
    env.hours = value
    echo env.hours

    // Берём количество минут для timeslot
    value = sh (script: "xmllint --xpath \'//MobileShop/tests/" + env.var + "/timeslot/minutes/text()\' MobileShop_Config.xml",,returnStdout:true).trim()
    env.minutes = value
    echo env.minutes

    /*
    // Описание теста
    value = powershell (returnStdout: true, script: '''
        # Открываем конфигурационный файл
        [xml]$configFile = Get-Content $ENV:WORKSPACE/MobileShop_Config.xml
        
        # Возвращаем параметр
        $output = $configFile.MobileShop.tests.$ENV:var.description
        Write-Output "$output)
    ''')
    env.description = value
    // echo env.description
    */

    // Параметры, которые в настоящее время имеют значения по умолчанию во всех тестах
    // Выполнять какое-либо действие после окончания теста?
    value = sh (script: "xmllint --xpath \'//MobileShop/tests/" + env.var + "/postRunAction/text()\' MobileShop_Config.xml",,returnStdout:true).trim()
    env.choisePostRunAction = value
    echo env.choisePostRunAction

    /* Trending
    // Использовать Trending
    value = powershell (returnStdout: true, script: '''
        # Открываем конфигурационный файл
        [xml]$configFile = Get-Content $ENV:WORKSPACE/MobileShop_Config.xml
        
        # Возвращаем параметр
        $output = $configFile.MobileShop.tests.$ENV:var.addRunToTrendReport
        Write-Output "$output)
    ''')
    env.addRunToTrendReport = value
    // echo env.addRunToTrendReport

    // Указываем id отчёта, если Trending используется с конкретным id
    value = powershell (returnStdout: true, script: '''
        # Открываем конфигурационный файл
        [xml]$configFile = Get-Content $ENV:WORKSPACE/MobileShop_Config.xml
        
        # Возвращаем параметр
        $output = $configFile.MobileShop.tests.$ENV:var.trendReportId
        Write-Output "$output)
    ''')
    env.trendReportId = value
    // echo env.trendReportId
    */

    // Указанеие экземпляра теста или его автоматическое определение
    value = sh (script: "xmllint --xpath \'//MobileShop/tests/" + env.var + "/autoTestInstanceId/text()\' MobileShop_Config.xml",,returnStdout:true).trim()
    env.autoTestInstanceId = value
    echo env.autoTestInstanceId

    /*
    // Указываем id экземпляра теста, если выбрано не автоматическое определение id
    value = powershell (returnStdout: true, script: '''
        # Открываем конфигурационный файл
        [xml]$configFile = Get-Content $ENV:WORKSPACE/MobileShop_Config.xml
        
        # Возвращаем параметр
        $output = $configFile.MobileShop.tests.$ENV:var.testInstanceId
        Write-Output "$output)
    ''')
    env.testInstanceId = value
    // echo env.testInstanceId
    */

    // Сравнить с SLA?
    value = sh (script: "xmllint --xpath \'//MobileShop/tests/" + env.var + "/statusBySLA/text()\' MobileShop_Config.xml",,returnStdout:true).trim()
    env.booleanStatusBySLA = value.toBoolean()
    echo env.booleanStatusBySLA.toString()

    // Использовать vudsMode?
    value = sh (script: "xmllint --xpath \'//MobileShop/tests/" + env.var + "/vudsMode/text()\' MobileShop_Config.xml",,returnStdout:true).trim()
    env.booleanVudsMode = value.toBoolean()
    echo env.booleanVudsMode.toString()
}

return this