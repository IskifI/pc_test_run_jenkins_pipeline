# Stopping_Kestrel
systemctl stop otus_loadqa

# Stopping_NGINX
nginx -s stop

# Checking_app_is_off
KESTREL=$(ps -ef | grep -c otus_loadqa)
if [[ $KESTREL > '3' ]]; then exit 1; fi
NGINX=$(ps -ef | grep -c nginx)
if [[ $NGINX > '3' ]]; then exit 1; fi

# Deleting_nginx_log
rm /var/log/nginx/*.log

# Checking_space_on_app
SPACE=$(df -h | grep -Ec '/dev/sda1(.)*[7-9][0-9]%')
if [[ $SPACE > '0' ]]; then exit 1; fi

# Starting_NGINX
nginx

# Starting_Kestrel
systemctl start otus_loadqa

# Collate_NGINX_logs
DATE=$(date +"%Y-%m-%d")
mkdir /home/userFolder/$DATE
cp /var/log/nginx/*.log /home/userFolder/$DATE

# Run_Jmeter_test
jmeter -n –t test.jmx

# Run_Gatling_test
$GATLING_HOME/bin/gatling.sh scenarioname

# Run_k6_test
k6 run script.js

# Run_Yandex.Tank_test
yandex-tank -c load.yaml ammo.txt

# Parallel_Test_Run_Pipeline *(groovy)
node('master') {  // выполняется на основной ноде Jenkins
    currentBuild.displayName = "${BUILD_TIMESTAMP}" //название билда
    currentBuild.result = 'SUCCESS'    // в другом месте скрипта можно заменить на 'FAILURE' чтобы весь пайп считался заваленным
    wrap([$class: 'TimestamperBuildWrapper']) {   //чтобы видеть время выполнения в консоли
        // этот stage, запустить два jobа параллельно
        stage('test'){ // название стадии, внутри блока parralel не должно быть stage
            parallel gatling_run_on_node_1: {  //название паралельного выполнения чтобы различать в отчете
            	build job: 'Test_Run_Jobs/Run_Gatling_test_node_1', parameters: [string(name: 'PARAM_NAME1', value: 'some value'),
            											  booleanParam(name: 'bool_PARAM_NAME', value: true)]
            }, gatling_run_on_node_2: {
            	build job: 'Test_Run_Jobs/Run_Gatling_test_node_2', parameters: [string(name: 'PARAM_NAME1', value: 'some value'),
            											  booleanParam(name: 'bool_PARAM_NAME', value: true)]
            }
        }
        // этот stage выполнится последовательно
        stage('printenv'){
        	sh 'printenv'  // печатаем в консоль доступные переменные окружения
    	}
    }
}