pipeline {
    agent any
    parameters {
        string(
            name: "SCENARIO_NAME",
            defaultValue: "maximum",
            description: "maximum confirmation, releability stress")
        
        string(
            name: "idt",
            defaultValue: "7",
            description: "Enter test ID")
    }

    stages {
        stage('MobileShop Restart app servers')
        {
            steps
            {
                timestamps
                {
                    echo 'Stopping_Application'
                    build job: 'Docker_MobileShop/Stoping_App_Container'
                    sh 'exit 0' // иначе если какойто из серверов уже выключен джоба фэйлится
                    sleep(10)
                }

                timestamps
                {
                    echo 'Checking_app_is_off'
                    build job: 'Docker_MobileShop/Checking_container_is_stop'
                    sleep(10)
                }

                timestamps
                {
                    echo 'Checking_space_on_app'
                    build job: 'Docker_MobileShop/Checking_space_on_app'
                    sleep(10)
                }

                timestamps
                {
                    echo 'Starting_container'
                    build job: 'Docker_MobileShop/Starting_container'
                    sleep(10)
                }

                timestamps
                {
                    echo 'Deleting_nginx_log'
                    build job: 'Docker_MobileShop/Deleting_nginx_log'
                    sleep(10)
                }
                
                timestamps
                {
                    echo 'Starting_Applications'
                    build job: 'Docker_MobileShop/Start_application_in_container'
                    sleep(10)
                }
            }
        }

        stage("Checkout GitLab repository")
        {
            steps
            {
                git credentialsId: '257253aa-1185-496d-b44c-f9608ad2326d', url: 'https://gitlab.com/IskifI/pc_test_run_jenkins_pipeline.git'
            }
        }

        stage("Run parameterized PC scenario")
        {
            steps
            {
                script
                {
                    def methods = load 'methods.groovy'
                    methods.getTestParams(env.SCENARIO_NAME)

                    try
                    {
                        withCredentials([usernamePassword(credentialsId: '6936613b-49b2-462e-ab05-5f19d72f0772', passwordVariable: 'PASSWORD', usernameVariable: 'USER')])
                        {
                            pcBuild httpsProtocol: booleanHttpsProtocol,
                            almDomain: env.almDomain,
                            almPassword: env.PASSWORD,
                            almProject: env.almProject,
                            almUserName: env.USER,
                            autoTestInstanceID: 'AUTO',
                            description: env.description,
                            pcServerName: env.pcServerName,
                            postRunAction: 'COLLATE',
                            proxyOutPassword: env.proxyOutPassword,
                            proxyOutUrl: env.proxyOutUrl,
                            proxyOutUser: env.proxyOutUser,
                            serverAndPort: env.serverAndPort,
                            statusBySLA: booleanStatusBySLA,
                            testId: env.idt,
                            testInstanceId: env.testInstanceId,
                            timeslotDurationHours: env.hours,
                            timeslotDurationMinutes: env.minutes,
                            trendReportId: env.trendReportId,
                            vudsMode: booleanVudsMode,
                            addRunToTrendReport: env.addRunToTrendReport
                        }
                    }
                    catch(all)
                    {
                        currentBuild.result = 'FAILURE'
                    }
                }
            }
        }

        stage("MobileShop Collate NGINX logs")
        {
            steps
            {
                timestamps
                {
                    echo 'Collate_NGINX_logs'
                    build job: 'Docker_MobileShop/Collate_NGINX_log'
                    sleep(10)
                }
            }
        }
    }
}